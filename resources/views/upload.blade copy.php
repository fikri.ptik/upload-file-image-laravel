
 
Home » Tutorial Laravel #30 : Membuat Upload File Laravel

Laravel
Tutorial Laravel #30 : Membuat Upload File Laravel
membuat upload file laravel
By Diki Alfarabi Hadi
27 March 2019
Laravel









Route::get('/upload', 'UploadController@upload');
Route::post('/upload/proses', 'UploadController@proses_upload');
Route::get('/upload', 'UploadController@upload');
Route::post('/upload/proses', 'UploadController@proses_upload');








<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadController extends Controller
{
	public function upload(){
		return view('upload');
	}

	public function proses_upload(Request $request){
		$this->validate($request, [
			'file' => 'required',
			'keterangan' => 'required',
		]);

		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');

      	        // nama file
		echo 'File Name: '.$file->getClientOriginalName();
		echo '<br>';

      	        // ekstensi file
		echo 'File Extension: '.$file->getClientOriginalExtension();
		echo '<br>';

      	        // real path
		echo 'File Real Path: '.$file->getRealPath();
		echo '<br>';

      	        // ukuran file
		echo 'File Size: '.$file->getSize();
		echo '<br>';

      	        // tipe mime
		echo 'File Mime Type: '.$file->getMimeType();

      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';

                // upload file
		$file->move($tujuan_upload,$file->getClientOriginalName());
	}
}
<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
class UploadController extends Controller
{
	public function upload(){
		return view('upload');
	}
 
	public function proses_upload(Request $request){
		$this->validate($request, [
			'file' => 'required',
			'keterangan' => 'required',
		]);
 
		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');
 
      	        // nama file
		echo 'File Name: '.$file->getClientOriginalName();
		echo '<br>';
 
      	        // ekstensi file
		echo 'File Extension: '.$file->getClientOriginalExtension();
		echo '<br>';
 
      	        // real path
		echo 'File Real Path: '.$file->getRealPath();
		echo '<br>';
 
      	        // ukuran file
		echo 'File Size: '.$file->getSize();
		echo '<br>';
 
      	        // tipe mime
		echo 'File Mime Type: '.$file->getMimeType();
 
      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
 
                // upload file
		$file->move($tujuan_upload,$file->getClientOriginalName());
	}
}



<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
	<div class="row">
		<div class="container">
			<h2 class="text-center my-5">Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</h2>
			
			<div class="col-lg-8 mx-auto my-5">	

				@if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif

				<form action="/upload/proses" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group">
						<b>File Gambar</b><br/>
						<input type="file" name="file">
					</div>

					<div class="form-group">
						<b>Keterangan</b>
						<textarea class="form-control" name="keterangan"></textarea>
					</div>

					<input type="submit" value="Upload" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 
</head>
<body>
	<div class="row">
		<div class="container">
			<h2 class="text-center my-5">Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</h2>
			
			<div class="col-lg-8 mx-auto my-5">	
 
				@if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
 
				<form action="/upload/proses" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
 
					<div class="form-group">
						<b>File Gambar</b><br/>
						<input type="file" name="file">
					</div>
 
					<div class="form-group">
						<b>Keterangan</b>
						<textarea class="form-control" name="keterangan"></textarea>
					</div>
 
					<input type="submit" value="Upload" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
</body>
</html>

<form action="/upload/proses" method="POST" enctype="multipart/form-data">
<form action="/upload/proses" method="POST" enctype="multipart/form-data">

public function proses_upload(Request $request){
	$this->validate($request, [
		'file' => 'required',
		'keterangan' => 'required',
	]);

	// menyimpan data file yang diupload ke variabel $file
	$file = $request->file('file');

      	// nama file
	echo 'File Name: '.$file->getClientOriginalName();
	echo '<br>';

      	// ekstensi file
	echo 'File Extension: '.$file->getClientOriginalExtension();
	echo '<br>';

      	// real path
	echo 'File Real Path: '.$file->getRealPath();
	echo '<br>';

      	// ukuran file
	echo 'File Size: '.$file->getSize();
	echo '<br>';

      	// tipe mime
	echo 'File Mime Type: '.$file->getMimeType();

      	// isi dengan nama folder tempat kemana file diupload
	$tujuan_upload = 'data_file';
	$file->move($tujuan_upload,$file->getClientOriginalName());
}
public function proses_upload(Request $request){
	$this->validate($request, [
		'file' => 'required',
		'keterangan' => 'required',
	]);
 
	// menyimpan data file yang diupload ke variabel $file
	$file = $request->file('file');
 
      	// nama file
	echo 'File Name: '.$file->getClientOriginalName();
	echo '<br>';
 
      	// ekstensi file
	echo 'File Extension: '.$file->getClientOriginalExtension();
	echo '<br>';
 
      	// real path
	echo 'File Real Path: '.$file->getRealPath();
	echo '<br>';
 
      	// ukuran file
	echo 'File Size: '.$file->getSize();
	echo '<br>';
 
      	// tipe mime
	echo 'File Mime Type: '.$file->getMimeType();
 
      	// isi dengan nama folder tempat kemana file diupload
	$tujuan_upload = 'data_file';
	$file->move($tujuan_upload,$file->getClientOriginalName());
}



// menyimpan data file yang diupload ke variabel $file
$file = $request->file('file');
// menyimpan data file yang diupload ke variabel $file
$file = $request->file('file');


// nama file
echo 'File Name: '.$file->getClientOriginalName();
echo '<br>';

// ekstensi file
echo 'File Extension: '.$file->getClientOriginalExtension();
echo '<br>';

// real path
echo 'File Real Path: '.$file->getRealPath();
echo '<br>';

// ukuran file
echo 'File Size: '.$file->getSize();
echo '<br>';

// tipe mime
echo 'File Mime Type: '.$file->getMimeType();
// nama file
echo 'File Name: '.$file->getClientOriginalName();
echo '<br>';
 
// ekstensi file
echo 'File Extension: '.$file->getClientOriginalExtension();
echo '<br>';
 
// real path
echo 'File Real Path: '.$file->getRealPath();
echo '<br>';
 
// ukuran file
echo 'File Size: '.$file->getSize();
echo '<br>';
 
// tipe mime
echo 'File Mime Type: '.$file->getMimeType();

// isi dengan nama folder tempat kemana file diupload
$tujuan_upload = 'data_file';
$file->move($tujuan_upload,$file->getClientOriginalName());
// isi dengan nama folder tempat kemana file diupload
$tujuan_upload = 'data_file';
$file->move($tujuan_upload,$file->getClientOriginalName());




















php artisan make:model Gambar --migration
php artisan make:model Gambar --migration

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGambarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gambar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gambar');
    }
}
<?php
 
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
 
class CreateGambarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gambar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->string('keterangan');
            $table->timestamps();
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gambar');
    }
}








<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
    protected $table = "gambar";

    protected $fillable = ['file','keterangan'];
}
<?php
 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Gambar extends Model
{
    protected $table = "gambar";
 
    protected $fillable = ['file','keterangan'];
}



<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
	<div class="row">
		<div class="container">

			<h2 class="text-center my-5">Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</h2>
			
			<div class="col-lg-8 mx-auto my-5">	

				@if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif

				<form action="/upload/proses" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group">
						<b>File Gambar</b><br/>
						<input type="file" name="file">
					</div>

					<div class="form-group">
						<b>Keterangan</b>
						<textarea class="form-control" name="keterangan"></textarea>
					</div>

					<input type="submit" value="Upload" class="btn btn-primary">
				</form>
				
				<h4 class="my-5">Data</h4>

				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="1%">File</th>
							<th>Keterangan</th>
							<th width="1%">OPSI</th>
						</tr>
					</thead>
					<tbody>
						@foreach($gambar as $g)
						<tr>
							<td><img width="150px" src="{{ url('/data_file/'.$g->file) }}"></td>
							<td>{{$g->keterangan}}</td>
							<td><a class="btn btn-danger" href="/upload/hapus/{{ $g->id }}">HAPUS</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 
</head>
<body>
	<div class="row">
		<div class="container">
 
			<h2 class="text-center my-5">Tutorial Laravel #30 : Membuat Upload File Dengan Laravel</h2>
			
			<div class="col-lg-8 mx-auto my-5">	
 
				@if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
 
				<form action="/upload/proses" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
 
					<div class="form-group">
						<b>File Gambar</b><br/>
						<input type="file" name="file">
					</div>
 
					<div class="form-group">
						<b>Keterangan</b>
						<textarea class="form-control" name="keterangan"></textarea>
					</div>
 
					<input type="submit" value="Upload" class="btn btn-primary">
				</form>
				
				<h4 class="my-5">Data</h4>
 
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="1%">File</th>
							<th>Keterangan</th>
							<th width="1%">OPSI</th>
						</tr>
					</thead>
					<tbody>
						@foreach($gambar as $g)
						<tr>
							<td><img width="150px" src="{{ url('/data_file/'.$g->file) }}"></td>
							<td>{{$g->keterangan}}</td>
							<td><a class="btn btn-danger" href="/upload/hapus/{{ $g->id }}">HAPUS</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gambar;

class UploadController extends Controller
{
	public function upload(){
		$gambar = Gambar::get();
		return view('upload',['gambar' => $gambar]);
	}

	public function proses_upload(Request $request){
		$this->validate($request, [
			'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
			'keterangan' => 'required',
		]);

		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');

		$nama_file = time()."_".$file->getClientOriginalName();

      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
		$file->move($tujuan_upload,$nama_file);

		Gambar::create([
			'file' => $nama_file,
			'keterangan' => $request->keterangan,
		]);

		return redirect()->back();
	}
}
<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use App\Gambar;
 
class UploadController extends Controller
{
	public function upload(){
		$gambar = Gambar::get();
		return view('upload',['gambar' => $gambar]);
	}
 
	public function proses_upload(Request $request){
		$this->validate($request, [
			'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
			'keterangan' => 'required',
		]);
 
		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');
 
		$nama_file = time()."_".$file->getClientOriginalName();
 
      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
		$file->move($tujuan_upload,$nama_file);
 
		Gambar::create([
			'file' => $nama_file,
			'keterangan' => $request->keterangan,
		]);
 
		return redirect()->back();
	}
}

use App\Gambar;
use App\Gambar;

public function upload(){
	$gambar = Gambar::get();
	return view('upload',['gambar' => $gambar]);
}
public function upload(){
	$gambar = Gambar::get();
	return view('upload',['gambar' => $gambar]);
}

public function proses_upload(Request $request){
	$this->validate($request, [
		'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
		'keterangan' => 'required',
	]);

	// menyimpan data file yang diupload ke variabel $file
	$file = $request->file('file');

	$nama_file = time()."_".$file->getClientOriginalName();

      	// isi dengan nama folder tempat kemana file diupload
	$tujuan_upload = 'data_file';
	$file->move($tujuan_upload,$nama_file);


	Gambar::create([
		'file' => $nama_file,
		'keterangan' => $request->keterangan,
	]);

	return redirect()->back();
}
public function proses_upload(Request $request){
	$this->validate($request, [
		'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
		'keterangan' => 'required',
	]);
 
	// menyimpan data file yang diupload ke variabel $file
	$file = $request->file('file');
 
	$nama_file = time()."_".$file->getClientOriginalName();
 
      	// isi dengan nama folder tempat kemana file diupload
	$tujuan_upload = 'data_file';
	$file->move($tujuan_upload,$nama_file);
 
 
	Gambar::create([
		'file' => $nama_file,
		'keterangan' => $request->keterangan,
	]);
 
	return redirect()->back();
}













SHARE :

Diki Alfarabi Hadi
Coding enthusiast. Someone who love learn something new. especially about web programming and web design. happy to share about knowledge and learn from other.

Author's profile All posts by Diki Alfarabi Hadi
Tags: aplikasi repository laravel, cara membuat upload file di laravel, cara upload file di laravel, cara upload file laravel, direktori laravel, laravel 5.5 upload image, laravel 5.6 upload image, laravel 5.8 upload image, laravel file upload validation, laravel image upload, laravel max size file upload validation, laravel upload, laravel upload file to storage, laravel upload image, laravel upload image to database, membuat upload file dengan laravel, membuat upload file laravel, membuat upload gambar dengan laravel, multiple file upload laravel 5, upload csv laravel, upload file laravel, upload file validation laravel, upload gambar di laravel, upload gambar laravel, upload multiple files laravel, upload xls laravel

PRODUK
Source Code Aplikasi Pengajuan Cuti Karyawan Berbasis WebsiteSource Code Aplikasi Pengajuan Cuti Karyawan Berbasis WebsiteRp 200,000
source code toko online sportSource Code Toko Sport PHP dan MySQLiRp 200,000
sistem informasi kuesioner berbasis webSource Code Sistem Informasi Kuesioner Berbasis WebRp 200,000
Source Code Sistem Informasi Survey Polling Mahasiswa Online PHP MySQLiRp 200,000
TUTORIAL TERBARU
Regular Expression Pada PHP September 18, 2021
Membuat Data Dummy di PHP Dengan Faker August 24, 2020
Menampilkan File PDF Di HTML Dengan Mudah August 12, 2020
Menampilkan Data Daerah Indonesia PHP MySQLi Ajax July 29, 2020
Membuat Bentuk Segitiga Dengan CSS July 7, 2020
TOKO KAMI
  
SOCIAL

TUTORIAL MENARIK LAINNYA
action url laravel
Laravel
Tutorial Laravel #41 : Action URL Laravel
19 April 2019
Action URL Laravel – Action URL adalah sebuah fitur pada laravel untuk tujuan generate URL ke controller, dan juga bisa mengirimkan data parameter seperti pada ...

Diki Alfarabi Hadi
laravel localization
Laravel
Tutorial Laravel #40 : Multi Bahasa Localization Laravel
14 April 2019
Multi Bahasa Localization Laravel – Multi bahasa merupakan fitur pada website untuk menampilkan informasi dalam berbagai bahasa. Laravel telah menyediakan fitur multi bahasa. sehingga untuk ...

Diki Alfarabi Hadi

Laravel
Tutorial Laravel #39 : Import Excel Laravel
12 April 2019
Import Excel Laravel – Setelah sebelumnya kita belajar tentang membuat export atau cetak laporan excel pada laravel, seperti ada yang kurang jika kita tidak membahas ...

Diki Alfarabi Hadi

Laravel
Tutorial Laravel #38 : Export Excel Laravel
12 April 2019
Export Excel Laravel – Fitur export atau cetak laporan excel merupakan salah satu fitur yang paling penting. bagi teman-teman yang sering membuat aplikasi pesanan dari ...

Diki Alfarabi Hadi
Diskusi3 Komentar
Tutorial Laravel #31 : Hapus File Dengan Laravel – Malas Ngoding 3 years ago
[…] masih akan melanjutkan materi pembelajaran kita sebelumnya tentang laravel dasar untuk pemula. pada Tutorial Laravel #30 sebelumnya, kita telah belajar tentang membuat upload file dengan laravel. dan menyimpan data yang […]

  Reply

jadzab crazy 3 years ago
mas, di database saya nyimpan gambar nya sebagai .tmp . bagaimana solusinya mas?

  Reply

Diki Alfarabi Hadi 3 years ago
lihat di codingannya mas coba cek

  Reply
Tulis Komentar / Pertanyaan
Your email address will not be published. Required fields are marked *

Comment

Name * 

Email * 

Website 


Download Ebook belajar HTML & CSS dasar untuk pemula gratis.
Ebook ini di buat oleh Diki Alfarabi Hadi, Founder dari www.malasngoding.com. yang aktif menulis tutorial pemrograman di www.malasngoding.com. ebook ini bisa di download oleh teman-teman yang baru mulai belajar HTML dan CSS dasar tapi tidak tahu mau memulai belajar dari mana. materi pembelajaran sudah di susun secara sistematis dan di dukung contoh gambar.

Shop Circle halo@malasngoding.com
     
© 2022 www.malasngoding.com with 

Cara Pemesanan Bukti Pengiriman Pasang Iklan Kontak